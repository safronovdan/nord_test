<?

abstract class Tree 
{ 
  // создает узел (если $parentNode == NULL - корень) 
  abstract protected function createNode(Node $node,$parentNode=NULL); 

  // удаляет узел и все дочерние узлы 
  abstract protected function deleteNode(Node $node); 

  // один узел делает дочерним по отношению к другому 
  abstract protected function attachNode(Node $node,Node $parent); 
  
  // получает узел по названию 
  abstract protected function getNode($nodeName); 

  // преобразует дерево со всеми элементами в ассоциативный массив 
  abstract protected function export(); 

} 


class Node 
{ 
	private $name; 
	private $parent_node=NULL;
	private $children=array();
		
	function __construct($name) { 
		$this->name = $name;
	}
	// делает переданный узел дочерним узлом данного
	function addChild(Node $node){
		$node->setParent($this);
		$this->children[]=$node;
				
	}
	
	//возвращает родительский узел
	public function getParent(){
		return $this->parent_node;
	}
	
	// возвращает имя узла
	public function getName(){
		return $this->name;
	}
	
	//делает текущий узел дочерним узлом переданного
	protected function setParent(Node $node){
		$this->parent_node=$node;
	}
	
	//возвращает дочерние узлы данного
	public function getChildren(){
		return $this->children;
	}
	
	//возвращает дочерний узел по его имени
	public function getChildByName($nodeName){

		
		foreach($this->getChildren() as $child){

			if($child->getName()==$nodeName){
				return $child;
			}else{
				
				$gchild =  $child->getChildByName($nodeName);
				if($gchild!=NULL){
					return $gchild;
				}
			
			}
		
		}
		
		return NULL;
	}
	
	//преобразует узел в ассоциативный массив
	public function toArray(){
					
			$parent = $this->getParent();

			$children = array();
			foreach($this->getChildren() as $child){
				$children[]=$child->toArray();
			}
			
			return array(
				"NAME"=>$this->getName(),
				"PARENT"=>$parent==NULL?$null:$parent->getName(),
				"CHILDREN"=>$children
			);		
		
	}
	
	//удаляет переданный дочерний узел
	public function deleteChild(Node $node){
		foreach ($this->children as $key=>$val){
			if($val==$node){
				unset($this->children[$key]);
			}
		}
	}
		
    
} 

class MyTreee extends Tree{
	
	protected $root;
	
	// создает узел (если $parentNode == NULL - корень) 	
	public function createNode(Node $node,$parentNode=NULL){
		
		if($parentNode != NULL){
			
			try{
				
				$parent = $this->getNode($parentNode->getName());
				$parent->addChild($node);
				
			}catch(Execption $e){
				throw new Exception("Can't add Node because ".$e->getMessage()."");
			}
		}else{
			
			$this->root = $node;
		}
		  
	} 

	// удаляет узел и все дочерние узлы 
	public function deleteNode(Node $node){
		if($this->root==$node){
			unset($this->root);
		}else{
			$parent = $node->getParent();
			$parent->deleteChild($node);
		}
		  
	} 

	// один узел делает дочерним по отношению к другому 
	// если переданный узел уже является дочерним узлом - вся данная ветка будет перенесена
	// так сделано чтобы не дублировались имена узлов
	// т.к как я понял по условию они уникальные иначе метод getNode($nodeName) теряет смысл либо
	// должен возвращать массив
	public function attachNode(Node $node,Node $parent){

		$old_parent = $node->getParent();
		if($old_parent!=NUll){
			//если узел уже есть то переставляем его 
			$old_parent->deleteChild($node);
		}
		$parent->addChild($node);		  
	}
	  
	// получает узел по названию 
	// если узла с переданным именем нет в дереве - кидает исключение
	// по хорошему нужно было во всех методах где используется данный добавить блок 
	// try - catch , но я поленился и добавил только в один
	public function getNode($nodeName){
		
		if ($this->root->getName()==$nodeName){
			return $this->root;
		}else{
			$treeNode=$this->root->getChildByName($nodeName);
			if($treeNode!=NULL){
				return $treeNode;
			}else{
				throw new Exception("Node with name \" $nodeName \" not found in the Tree;");
			}
			
		}
				  
	}

	// преобразует дерево со всеми элементами в ассоциативный массив 
	public function export(){
		return $this->root->toArray();	  
	}
  
}

//TEST
$tree = new MyTreee();
// 1. создать корень country 
$tree->createNode(new Node('country')); 
// 2. создать в нем узел kiev 
$tree->createNode(new Node('kiev'), $tree->getNode('country')); 
// 3. в узле kiev создать узел kremlin 
$tree->createNode(new Node('kremlin'), $tree->getNode('kiev')); 
// 4. в узле kremlin создать узел house 
$tree->createNode(new Node('house'), $tree->getNode('kremlin')); 
// 5. в узле kremlin создать узел tower 
$tree->createNode(new Node('tower'), $tree->getNode('kremlin')); 
// 4. в корневом узле создать узел moskow 
$tree->createNode(new Node('moskow'), $tree->getNode('country')); 
// 5. сделать узел kremlin дочерним узлом у moskow 
$tree->attachNode($tree->getNode('kremlin'), $tree->getNode('moskow')); 
// 6. в узле kiev создать узел maidan 
$tree->createNode(new Node('maidan'), $tree->getNode('kiev')); 
// 7. удалить узел kiev 
$tree->deleteNode($tree->getNode('kiev')); 
// 8. получить дерево в виде массива, сделать print_r 
echo "<pre>";
print_r($tree->export()); 
echo "<pre>";

