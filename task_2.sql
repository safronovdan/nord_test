--Знания MySQL + оптимизировать запросы

--Имеется 3 таблицы: info, data, link, есть запрос для получения данных:
select 
	* 
from 
	data
	,link
	,info 
where 
	link.info_id = info.id 
	and link.data_id = data.id

--предложить варианты оптимизации.

-- я бы добавли индекс в таблицу link :
CREATE INDEX link_ind
        ON link (data_id,info_id)
		
-- либо добавил бы туда первичный ключ :
ALTER TABLE link  ADD PRIMARY KEY (data_id,info_id)

-- кроме того в запросе не использовал бы * а использовал бы только требуемые поля


--Запросы для создания таблиц:

CREATE TABLE `info` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) default NULL,
  `desc` text default NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

CREATE TABLE `data` (
  `id` int(11) NOT NULL auto_increment,
  `date` date default NULL,
  `value` INT(11) default NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

CREATE TABLE `link` (
  `data_id` int(11) NOT NULL,
  `info_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;
